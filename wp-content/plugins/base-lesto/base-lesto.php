<?php

/**
 * Plugin Name: Base Lesto Escritórios
 * Description: Controle base do tema Lesto Escritórios.
 * Version: 0.1
 * Author: Agência Pixele
 * Author URI: http://www.pixele.com.br
 * Licence: GPL2
 */

	function baseLesto () {

		// TIPOS DE CONTEÚDO
		conteudosLesto();

		// TAXONOMIA
		taxonomiaLesto();

		// META BOXES
		metaboxesLesto();

		// SHORTCODES
		shortcodesLesto();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosLesto (){

		// TIPOS DE CONTEÚDO
		tipoDestaques();
		
		tipoPlanos();
		// tipoVantagens();
		tipoDepoimentos();

		tipoSalas();

		// ALTERAÇÃO DO TÍTULO PADRÃO
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );

		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaques':
					$titulo = 'Título do destaque';
				break;

				case 'Lesto-brasil':
					$titulo = 'Nome do item';
				break;

				case 'cursos':
					$titulo = 'Nome do curso';
				break;

				case 'agenda':
					$titulo = 'Título do agendamento';
				break;

				default:

				break;
			}

	     	return $titulo;

		}

	}

		// CONTEÚDO: DESTAQUES
		function tipoDestaques() {

				$rotulos 			= array(
										'name'               => 'Destaques',
										'singular_name'      => 'Destaque',
										'menu_name'          => 'Destaques',
										'name_admin_bar'     => 'Destaque',
										'add_new'            => 'Adicionar novo',
										'add_new_item'       => 'Adicionar novo destaque',
										'new_item'           => 'Novo destaque',
										'edit_item'          => 'Editar destaque',
										'view_item'          => 'Ver destaques',
										'all_items'          => 'Todos destaques',
										'search_items'       => 'Buscar destaques',
										'parent_item_colon'  => 'Dos destaques',
										'not_found'          => 'Nenhum destaque cadastrado.',
										'not_found_in_trash' => 'Nenhum destaque na lixeira.'
									);

				$args 				= array(
										'labels'             => $rotulos,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 4,
										'menu_icon'          => 'dashicons-megaphone',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'destaques' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title', 'editor', 'thumbnail' )
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('destaques', $args);

		}

		// CONTEÚDO: PLANOS
		function tipoPlanos() {

				$rotulos 			= array(
										'name'               => 'Planos',
										'singular_name'      => 'Plano',
										'menu_name'          => 'Planos',
										'name_admin_bar'     => 'Plano',
										'add_new'            => 'Adicionar novo',
										'add_new_item'       => 'Adicionar novo plano',
										'new_item'           => 'Novo plano',
										'edit_item'          => 'Editar plano',
										'view_item'          => 'Ver planos',
										'all_items'          => 'Todos planos',
										'search_items'       => 'Buscar planos',
										'parent_item_colon'  => 'Dos planos',
										'not_found'          => 'Nenhum plano cadastrado.',
										'not_found_in_trash' => 'Nenhum plano na lixeira.'
									);

				$args 				= array(
										'labels'             => $rotulos,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 4,
										'menu_icon'          => 'dashicons-screenoptions',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'planos' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title', 'editor','thumbnail' )
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('planos', $args);

		}

		// CONTEÚDO: VANTANGENS
		function tipoVantagens() {

				$rotulos 			= array(
										'name'               => 'Vantagens',
										'singular_name'      => 'Vantagem',
										'menu_name'          => 'Vantagens',
										'name_admin_bar'     => 'Vantagem',
										'add_new'            => 'Adicionar nova',
										'add_new_item'       => 'Adicionar nova vantagem',
										'new_item'           => 'Nova vantagem',
										'edit_item'          => 'Editar vantagem',
										'view_item'          => 'Ver vantagens',
										'all_items'          => 'Todas vantagens',
										'search_items'       => 'Buscar vantagens',
										'parent_item_colon'  => 'Das vantagens',
										'not_found'          => 'Nenhuma vantagem cadastrada.',
										'not_found_in_trash' => 'Nenhuma vantagem na lixeira.'
									);

				$args 				= array(
										'labels'             => $rotulos,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 4,
										'menu_icon'          => 'dashicons-star-filled',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'vantagens' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title', 'editor' )
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('vantagens', $args);

		}

		// CONTEÚDO: DEPOIMENTOS
		function tipoDepoimentos() {

				$rotulosDepoimentos = array(
										'name'               => 'Depoimentos',
										'singular_name'      => 'Depoimento',
										'menu_name'          => 'Depoimentos',
										'name_admin_bar'     => 'Depoimento',
										'add_new'            => 'Adicionar novo',
										'add_new_item'       => 'Adicionar novo depoimento',
										'new_item'           => 'Novo depoimento',
										'edit_item'          => 'Editar depoimento',
										'view_item'          => 'Ver depoimentos',
										'all_items'          => 'Todos depoimentos',
										'search_items'       => 'Buscar depoimentos',
										'parent_item_colon'  => 'Dos depoimentos',
										'not_found'          => 'Nenhum depoimento cadastrado.',
										'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
									);

				$argsDepoimentos 	= array(
										'labels'             => $rotulosDepoimentos,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 8,
										'menu_icon'          => 'dashicons-id-alt',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'depoimentos' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title', 'editor', 'thumbnail' )
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('depoimentos', $argsDepoimentos);

		}

		// CONTEÚDO: SALAS
		function tipoSalas() {

				$rotulosSalas = array(
										'name'               => 'sala',
										'singular_name'      => 'Sala',
										'menu_name'          => 'Salas',
										'name_admin_bar'     => 'Sala',
										'add_new'            => 'Adicionar nova',
										'add_new_item'       => 'Adicionar nova sala',
										'new_item'           => 'Nova sala',
										'edit_item'          => 'Editar sala',
										'view_item'          => 'Ver sala',
										'all_items'          => 'Todas as salas',
										'search_items'       => 'Buscar sala',
										'parent_item_colon'  => 'Das salas',
										'not_found'          => 'Nenhuma sala cadastrado.',
										'not_found_in_trash' => 'Nenhuma sala na lixeira.'
									);

				$argsSalas 	= array(
										'labels'             => $rotulosSalas,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 8,
										'menu_icon'          => 'dashicons-admin-home',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'salas' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title','thumbnail' )
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('salas', $argsSalas);

		}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaLesto () {

		taxCategoriaAgenda();
		taxCategoriaClipping();
		taxCategoriaSalas();

	}

		// TAXONOMIA: CATEGORIA DE AGENDA
		function taxCategoriaAgenda() {

				$rotulos 			= array(
										'name'              => 'Categorias de agenda',
										'singular_name'     => 'Categoria de agenda',
										'search_items'      => 'Buscar categorias',
										'all_items'         => 'Todas categorias',
										'parent_item'       => 'Categoria pai',
										'parent_item_colon' => '',
										'edit_item'         => 'Editar categoria',
										'update_item'       => 'Atualizar categoria',
										'add_new_item'      => 'Adicionar nova categoria',
										'new_item_name'     => 'Nova categoria',
										'menu_name'         => 'Categorias',
				);

				$args 				= array(
										'hierarchical'      => true,
										'labels'            => $rotulos,
										'show_ui'           => true,
										'show_admin_column' => true,
										'query_var'         => true,
										'rewrite'           => array( 'slug' => 'categoria-agenda' ),
				);

			register_taxonomy( 'categoriaAgenda', array( 'agenda' ), $args );
		}

		// TAXONOMIA: CATEGORIA DE CLIPPINGS
		function taxCategoriaClipping() {

				$rotulos 			= array(
										'name'              => 'Categorias de clipping',
										'singular_name'     => 'Categoria de clipping',
										'search_items'      => 'Buscar categorias',
										'all_items'         => 'Todas categorias',
										'parent_item'       => 'Categoria pai',
										'parent_item_colon' => '',
										'edit_item'         => 'Editar categoria',
										'update_item'       => 'Atualizar categoria',
										'add_new_item'      => 'Adicionar nova categoria',
										'new_item_name'     => 'Nova categoria',
										'menu_name'         => 'Categorias',
				);

				$args 				= array(
										'hierarchical'      => true,
										'labels'            => $rotulos,
										'show_ui'           => true,
										'show_admin_column' => true,
										'query_var'         => true,
										'rewrite'           => array( 'slug' => 'categoria-clipping' ),
				);

			register_taxonomy( 'categoriaClipping', array( 'clippings' ), $args );
		}

		// TAXONOMIA: CATEGORIA DE SALAS
		function taxCategoriaSalas() {

				$rotulos 			= array(
										'name'              => 'Categorias de salas',
										'singular_name'     => 'Categoria de sala',
										'search_items'      => 'Buscar categorias',
										'all_items'         => 'Todas categorias',
										'parent_item'       => 'Categoria pai',
										'parent_item_colon' => '',
										'edit_item'         => 'Editar categoria',
										'update_item'       => 'Atualizar categoria',
										'add_new_item'      => 'Adicionar nova categoria',
										'new_item_name'     => 'Nova categoria',
										'menu_name'         => 'Categorias',
				);

				$args 				= array(
										'hierarchical'      => true,
										'labels'            => $rotulos,
										'show_ui'           => true,
										'show_admin_column' => true,
										'query_var'         => true,
										'rewrite'           => array( 'slug' => 'categoria-salas' ),
				);

			register_taxonomy( 'categoriasalas', array( 'salas' ), $args );
		}


    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesLesto(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Lesto_';

			// 1st meta box
			$metaboxes[] = array(
				// Meta box id, UNIQUE per meta box. Optional since 4.1.5
				'id' => 'standard',

				// Meta box title - Will appear at the drag and drop handle bar. Required.
				'title' => __( 'Standard Fields', 'rwmb' ),

				// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
				'pages' => array( 'teste' ),

				// Where the meta box appear: normal (default), advanced, side. Optional.
				'context' => 'normal',

				// Order of meta box: high (default), low. Optional.
				'priority' => 'high',

				// Auto save: true, false (default). Optional.
				'autosave' => true,

				// List of meta fields
				'fields' => array(
					// TEXT
					array(
						// Field name - Will be used as label
						'name'  => __( 'Text', 'rwmb' ),
						// Field ID, i.e. the meta key
						'id'    => "{$prefix}text",
						// Field description (optional)
						'desc'  => __( 'Text description', 'rwmb' ),
						'type'  => 'text',
						// Default value (optional)
						'std'   => __( 'Default text value', 'rwmb' ),
						// CLONES: Add to make the field cloneable (i.e. have multiple value)
						'clone' => true,
					),
					// CHECKBOX
					array(
						'name' => __( 'Checkbox', 'rwmb' ),
						'id'   => "{$prefix}checkbox",
						'type' => 'checkbox',
						// Value can be 0 or 1
						'std'  => 1,
					),
					// RADIO BUTTONS
					array(
						'name'    => __( 'Radio', 'rwmb' ),
						'id'      => "{$prefix}radio",
						'type'    => 'radio',
						// Array of 'value' => 'Label' pairs for radio options.
						// Note: the 'value' is stored in meta field, not the 'Label'
						'options' => array(
							'value1' => __( 'Label1', 'rwmb' ),
							'value2' => __( 'Label2', 'rwmb' ),
						),
					),
					// SELECT BOX
					array(
						'name'     => __( 'Select', 'rwmb' ),
						'id'       => "{$prefix}select",
						'type'     => 'select',
						// Array of 'value' => 'Label' pairs for select box
						'options'  => array(
							'value1' => __( 'Label1', 'rwmb' ),
							'value2' => __( 'Label2', 'rwmb' ),
						),
						// Select multiple values, optional. Default is false.
						'multiple'    => false,
						'std'         => 'value2',
						'placeholder' => __( 'Select an Item', 'rwmb' ),
					),
					// HIDDEN
					array(
						'id'   => "{$prefix}hidden",
						'type' => 'hidden',
						// Hidden field must have predefined value
						'std'  => __( 'Hidden value', 'rwmb' ),
					),
					// PASSWORD
					array(
						'name' => __( 'Password', 'rwmb' ),
						'id'   => "{$prefix}password",
						'type' => 'password',
					),
					// TEXTAREA
					array(
						'name' => __( 'Textarea', 'rwmb' ),
						'desc' => __( 'Textarea description', 'rwmb' ),
						'id'   => "{$prefix}textarea",
						'type' => 'textarea',
						'cols' => 20,
						'rows' => 3,
					),
				),
				'validation' => array(
					'rules' => array(
						"{$prefix}password" => array(
							'required'  => true,
							'minlength' => 7,
						),
					),
					// optional override of default jquery.validate messages
					'messages' => array(
						"{$prefix}password" => array(
							'required'  => __( 'Password is required', 'rwmb' ),
							'minlength' => __( 'Password must be at least 7 characters', 'rwmb' ),
						),
					)
				)
			);

			// DESTAQUES
			$metaboxes[] = array(

				'id'			=> 'destaquesMetabox',
				'title'			=> 'Dados do destaque',
				'pages' 		=> array( 'destaques' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Item',
						'id'    => "{$prefix}destaque_item",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
						'name'             => 'Imagem esquerda',
						'id'               => "{$prefix}destaque_imagem1",
						'type'             => 'plupload_image',
						'max_file_uploads' => 1,
					),

					array(
						'name'             => 'Imagem direita',
						'id'               => "{$prefix}destaque_imagem2",
						'type'             => 'plupload_image',
						'max_file_uploads' => 1,
					),

					array(
						'name'             => 'Miniatura',
						'id'               => "{$prefix}destaque_miniatura",
						'type'             => 'plupload_image',
						'max_file_uploads' => 1,
					),

					array(
						'name'  => 'Valor',
						'id'    => "{$prefix}destaque_valor",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
						'name'  => 'Frequência',
						'id'    => "{$prefix}destaque_valor_frequencia",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
						'name'  => 'Link do destaque',
						'id'    => "{$prefix}destaque_link",
						'desc'  => '',
						'type'  => 'text'
					),

				),
				'validation' 	=> array()
			);

			// PLANOS
			$metaboxes[] = array(

				'id'			=> 'planosMetabox',
				'title'			=> 'Detalhes do plano',
				'pages' 		=> array( 'planos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Itens do plano',
						'id'    => "{$prefix}plano_items",
						'desc'  => '',
						'type'  => 'text',
						'clone' => true
					),
					array(
						'name'  => 'Título itens adicionais do plano',
						'id'    => "{$prefix}plano_titulo_items2",
						'desc'  => '',
						'type'  => 'text',
						'clone' => true
					),
					array(
						'name'  => 'Itens adicionais do plano',
						'id'    => "{$prefix}plano_items2",
						'desc'  => '',
						'type'  => 'text',
						'clone' => true
					),
					array(
						'name'  => 'Valor',
						'id'    => "{$prefix}plano_valor",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Frequência',
						'id'    => "{$prefix}plano_frequencia",
						'desc'  => 'Ex: /mês',
						'type'  => 'text'
					)/*,
					array(
						'name'             => 'Imagem (miniatura)',
						'id'               => "{$prefix}plano_imagem1",
						'type'             => 'image',
						'max_file_uploads' => 1,
					),

					array(
						'name'             => 'Imagem (grande)',
						'id'               => "{$prefix}plano_imagem2",
						'type'             => 'image',
						'max_file_uploads' => 1,
					),*/

				),
				'validation' 	=> array()
			);

			//DEPOIMENTOS
			$metaboxes[] = array(

				'id'			=> 'salasMetabox',
				'title'			=> 'Detalhes da sala',
				'pages' 		=> array( 'salas' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Valor por hora para cliente interno',
						'id'    => "{$prefix}sala_valor1",
						'desc'  => 'Valor por hora para cliente interno',
						'type'  => 'text'
					),
					array(
						'name'  => 'Valor por hora para cliente externo',
						'id'    => "{$prefix}sala_valor2",
						'desc'  => 'Valor por hora para cliente externo',
						'type'  => 'text'
					),
					array(
						'name'  => 'Imagem galeria',
						'id'    => "{$prefix}img_galeria",
						'type'  => 'image',
						'max_file_uploads'  => 1,
					),		

				),
				'validation' 	=> array()
			); 

			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesLesto(){

		add_shortcode( 'lista-cursos', 			'sc_listaCursos' );
		add_shortcode( 'lista-cursos-verde', 	'sc_listaCursosVerde' );
		add_shortcode( 'atendimento', 			'sc_atendimento' );

	}
		// SHORTCODE: LISTA CURSOS
		function sc_listaCursos(){

			$listaCursos 		= array();

			$output = '<ul class="lista-cursos">';

			$loopListaCursos 	= new WP_Query( array( 'post_type' => 'cursos', 'posts_per_page' => -1 ) );
			$i = 1;
			while ( $loopListaCursos->have_posts() ) : $loopListaCursos->the_post();

			global $post;

				$requisitos = (rwmb_meta('Lesto_curso_requisitos') == '') ? 'Não há pré-requisitos' : rwmb_meta( 'Lesto_curso_requisitos',array('multiple' => true) );

				if(is_array($requisitos)){
					$r = '';
					$x = 0;
					foreach ($requisitos as $requisito) {
						$x++;
						$requisito = explode('|', $requisito);
						if($x == count($requisitos)){
							$r .= $requisito[1];
						}else {
							$r .= $requisito[1] . ' + ';
						}
					}
					$requisitos = $r;
				}

				$output .= '<li class="curso-' . $i . '"><a href="' . get_the_permalink() . '" class="ico-curso ' . $post->post_name . '" title="' . get_the_title() . ' - Pré-requisitos: ' . $requisitos . '"></a></li>';
                $i++;

            endwhile;

            wp_reset_query();

            $output .= '</ul>';

	        return $output;

	    }

		// SHORTCODE: LISTA CURSOS
		function sc_listaCursosVerde(){

			$listaCursos 		= array();

			$output = '<ul class="lista-cursos">';

			$loopListaCursos 	= new WP_Query( array( 'post_type' => 'cursos', 'posts_per_page' => -1 ) );
			$i = 1;
			while ( $loopListaCursos->have_posts() ) : $loopListaCursos->the_post();

			global $post;

				$requisitos = (rwmb_meta('Lesto_curso_requisitos') == '') ? 'Não há pré-requisitos' : rwmb_meta( 'Lesto_curso_requisitos',array('multiple' => true) );

				if(is_array($requisitos)){
					$r = '';
					$x = 0;

					foreach ($requisitos as $requisito) {
						$x++;
						$requisito = explode('|', $requisito);
						if($x == count($requisitos)){
							$r .= $requisito[1];
						}else {
							$r .= $requisito[1] . ' + ';
						}
					}
					$requisitos = $r;
				}

				$output .= '<li class="curso-' . $i . '"><a href="' . get_the_permalink() . '" class="ico-curso verde ' . $post->post_name . '" title="' . get_the_title() . ' - Pré-requisitos: ' . $requisitos . '"></a></li>';
                $i++;

            endwhile;

            wp_reset_query();

            $output .= '</ul>';

	        return $output;

	    }

		// SHORTCODE: ATENDIMENTO
		function sc_atendimento(){

			global $configuracao;

			$listaCursos 		= array();

			$output = '<div class="row">
	                      <div class="col-md-offset-2 col-md-8">
	                        <span class="enfase-titulo vermelho">atendimentos apenas com hora marcada</span>
	                      </div>
	                    </div>

	                    <div class="row">
	                      <div class="col-md-6 bloco vermelho" id="info-localizacao">
	                        <div id="info-loc1" style="dislay: ;">
	                          <span class="subtitulo" style="visibility: hidden;">.</span>
	                          <span class="titulo">Localização</span>
	                          <span class="subtitulo">onde ser atendido</span>
	                        </div>
	                        <div id="info-loc2" style="display: none; padding: 20px 10px; text-align: left;">
	                          <p class="loc"><span>RJ</span><img src="' . get_template_directory_uri() . '/img/ico_seta-esq.png" /><i>' . $configuracao['opt-endereco'] . '<br /> ' . $configuracao['opt-bairro'] . ' - [ <a href="' . home_url('/contato') . '">mapa</a> ]</i></p>
	                          <p class="loc"><span>SP</span><img src="' . get_template_directory_uri() . '/img/ico_seta-esq.png" /><i>Em breve</i></p>
	                          <p class="loc"><span>BH</span><img src="' . get_template_directory_uri() . '/img/ico_seta-esq.png" /><i>Em breve</i></p>
	                          <p class="loc"><i style="margin-top: 10px;">Qualquer Localidade</i><img src="' . get_template_directory_uri() . '/img/ico_skype.png" style="margin-top: -35px; float: right;" /><img src="' . get_template_directory_uri() . '/img/ico_seta-dir.png" style="margin-top: -20px; float: right;" /></p>
	                        </div>
	                      </div>
	                      <div class="col-md-6 bloco vermelho">
	                        <div>
	                          <span class="subtitulo">marque uma consulta</span>
	                          <span class="titulo">onde ser atendido</span>
	                          <small class="txt-cinza"><a href="' . home_url('/contato') . '">preencha o formulário ou faça contato telefônico</a></small>
	                        </div>
	                      </div>
	                    </div>';

	        return $output;

	    }

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerLesto(){

	    if (class_exists('WPBakeryVisualComposer')){

	    	// VC: DEPOIMENTOS
		    function vc_depoimentos() {

		        vc_map( array(
		            "name" 		=> 'Depoimentos',
		            "base"		=> 'depoimentos',
		            "category"  => 'Lesto'
		        ) );
		    }

		    vc_depoimentos();
		}

	}

	// OUTRAS FUNÇÕES
	function listarCursos(){

		$listaCursos 		= array();


		$loopListaCursos 	= new WP_Query( array( 'post_type' => 'cursos', 'posts_per_page' => -1 ) );
		$i = 0;
		while ( $loopListaCursos->have_posts() ) : $loopListaCursos->the_post();

			global $post;

			$listaCursos[$post->post_name] = get_the_title();

        endwhile;

        wp_reset_query();

        return $listaCursos;
	}


  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseLesto');

	// FLUSHS
	function rewrite_flush() {

    	baseLesto();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );