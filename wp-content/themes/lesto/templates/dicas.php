<?php
/**
 * Template Name: Dicas
 * Description: 
 *
 * @package lesto
 */

get_header(); ?>


	<!-- DICAS -->
	<div class="pg-dicas internas container">
		<h3>Dicas</h3>
		<div class="row interna-conteudo" style="background-color: transparent;">
			<div class="col-md-12">

				<div class="text-center">
					<p class="pagina-descricao text-center"> Proin gravida a nisl et accumsan. Praesent pulvinar, ligula ac porta accumsan, velit ligula laoreet nibh, non egestas magna diam a metus. Ut vel libero nunc.</b></p>
					<span class="glyphicon glyphicon-chevron-down" style="font-size: 2em;"></span>
					<br /><br />
				</div>

				<div class="row">
					<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post(); ?>

						  <?php
						  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
						  $url = $thumb['0'];
						  ?>

							<div class="col-md-6">
								<div class="dica">
									<figure style="background-image: url('http://placehold.it/400x200');">
										<figcaption class="dica-data">
											<span class="dia"><?php echo  mysql2date('d', $post->post_date); ?></span>
											<span class="mes"><?php echo  strtoupper(mysql2date('M', $post->post_date)); ?></span>
										</figcaption>
									</figure>
									<h2><?php echo get_the_title(); ?></h2>
									<p><?php echo get_the_content(); ?></p>
									<a href="#" class="dica-link pull-right">Ler mais</a>
									<div class="clear"></div>
								</div>
							</div>

						<?php endwhile; ?>

						<?php the_posts_navigation(); ?>

					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
