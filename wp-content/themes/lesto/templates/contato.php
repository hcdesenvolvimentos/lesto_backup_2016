<?php
/**
 * Template Name: Contato
 * Description:
 *
 * @package lesto
 */

get_header();

?>

	<!-- CONTATO -->
	<div class="pg-contato internas container">
		<h3>Contato</h3>
		<div class="row interna-conteudo">
			<div class="col-md-12">

				<p class="pagina-descricao text-center"><?php echo get_the_content(); ?></p>

				<div class="row">
					<div class="col-md-6">
						<div class="alert alert-success" style="display: none;">
							<p>Obrigado pelo interesse, logo retornaremos seu contato.</p>
						</div>
						<div class="formulario-contato">
							<span>Formulário de contato</span>
							<label class="naoalinhado">
								<input type="text" placeholder="Seu nome" id="contatoNome" />
							</label>
							<label class="naoalinhado">
								<input type="text" placeholder="Seu e-mail" id="contatoEmail" />
							</label>
							<label class="naoalinhado">
								<input type="text" placeholder="Seu telefone" id="contatoTelefone" />
							</label>
							<label class="naoalinhado">
								<select id="contatoUnidade">
									<option value="">Selecione a unidade de interesse</option>
	                                <option value="Unidade Centro Cívico Neo Bussiness">Unidade Centro Cívico Neo Bussiness</option>
	                                <option value="Unidade Trajano Reis, 472">Unidade Trajano Reis, 472</option>
								</select>
							</label>
							<label class="naoalinhado">
								<textarea rows="6" id="contatoMensagem"></textarea>
							</label>
							<button class="btn btn-lg btn-verde center-block" id="enviar-contato">Enviar</button>
						</div>
					</div>
					<div class="col-md-6 text-center">
						<div class="contato-formas">
							<p>ou se preferir</p>
							<span class="contato-email"><?php echo $configuracao['opt-email']; ?></span>
							<span class="contato-telefone"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt-telefone']; ?> – Unidade Trajano</span>
							<span class="contato-telefone"><i class="fa fa-whatsapp" aria-hidden="true"></i>  (41) 98725-8970 – Unidade Trajano</span>
							<span class="contato-telefone"><i class="fa fa-phone" aria-hidden="true"></i>  (41) 3122-2057 – Unidade Neo</span>
							<span class="contato-telefone"><i class="fa fa-whatsapp" aria-hidden="true"></i>  (41) 99533-8368 – Unidade Neo</span>
						</div>
						<img src="<?php echo get_template_directory_uri(); ?>/img/ico_contato.png" />
					</div>
				</div>

			</div>
		</div>
	</div>

	<script>

        (function($) {

          $('#enviar-contato').click(function(){

            var contatoNome         = $('#contatoNome').val();
            var contatoEmail        = $('#contatoEmail').val();
            var contatoTelefone     = $('#contatoTelefone').val();
            var contatoUnidade      = $('#contatoUnidade').val();
            var contatoMensagem     = $('#contatoMensagem').val();

            if(contatoNome != '' && contatoEmail != ''){

              
              $(this).text('Enviando, aguarde...');

              // AJAX
              $.ajax({
                  url: '/site/wp-admin/admin-ajax.php',
                  type: 'POST',
                  data : {action: "formularioContato", contatoNome : contatoNome, contatoEmail: contatoEmail, contatoTelefone: contatoTelefone, contatoUnidade: contatoUnidade, contatoMensagem: contatoMensagem},
                  success: function (resp) {

                        $('.alert').fadeIn();

                        $('#enviar-contato').removeAttr('disabled');
                        $('#enviar-contato').html('Enviar');
                        $('.formulario-contato').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/992589158/?label=hPorCM6X1lYQ5uqm2QM&amp;guid=ON&amp;script=0"/>');

                  }

              });

            }else {
              alert('Certifique-se de nos informar seu nome e e-mail.');
            }

          });

        })(jQuery);

    </script>

<?php get_footer(); ?>