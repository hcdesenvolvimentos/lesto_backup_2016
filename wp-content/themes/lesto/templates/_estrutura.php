<?php
/**
 * Template Name: Estrutura
 * Description:
 *
 * @package lesto
 */

get_header();

global $configuracao;

?>
<div class="popup-gallery">
	<a href="<?php echo get_template_directory_uri(); ?>/img/IMG-20170316-WA0011.jpg" class="link-galeria"></a>
	<a href="<?php echo get_template_directory_uri(); ?>/img/WA0002.jpg" class="link-galeria"></a>
	<a href="<?php echo get_template_directory_uri(); ?>/img/ga52.png" class="link-galeria"></a>
	<a href="<?php echo get_template_directory_uri(); ?>/img/ga140.png" class="link-galeria"></a>
</div>

<div class="popup-gallery2">
	<a href="<?php echo get_template_directory_uri(); ?>/img/g186.png" class="link-galeria2"></a>
	<!-- <a href="<?php echo get_template_directory_uri(); ?>/img/ga248.png" class="link-galeria2"></a> -->
	<a href="<?php echo get_template_directory_uri(); ?>/img/ga195.png" class="link-galeria2"></a>
	<a href="<?php echo get_template_directory_uri(); ?>/img/g150.png" class="link-galeria2"></a>
</div>

	<!-- CONHEÇA NOSSA ESTRUTURA -->
	<div class="pg-estrutura internas container">
		<h3>Conheça nossa estrutura</h3>
		<div class="row interna-conteudo" style="padding-left: 0;">
			<div class="col-md-12">

				<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post(); ?>

				<div class="row unidade">
					<div class="col-md-6">
						<img src="<?php echo get_template_directory_uri(); ?>/img/neoestrutura.png" style="width: 100%; max-width: 592px; cursor: pointer;" id="botao" class="colorbox-30" /><!-- </a> -->
					</div>
					<div class="col-md-6">
						<div class="box">
							<h2>Unidade Centro Cívico Neo Business</h2>

							<?php
						        $gallery = get_post_gallery();
							 ?>

							 <ul>
							 	<?php foreach ($configuracao['opt-informacoes-neo'] as $info){ ?>

							 		<li> <?= $info;?> </li>

							 	<?php }?>
							 </ul>

						</div>
					</div>
				</div>

				<div class="row unidade">
					<div class="col-md-6">
						<img src="<?php echo get_template_directory_uri(); ?>/img/trajanoEstrutura.png" style="width: 100%; max-width: 592px; cursor: pointer;" id="botao2" class="colorbox-30" /><!-- </a> -->
					</div>
					<div class="col-md-6">
						<div class="box">
							<h2>Unidade Trajano Reis</h2>
							<?php
						        $gallery = get_post_gallery();
							 ?>


							 <ul>
							 	<?php foreach ($configuracao['opt-informacoes-trajano'] as $info){ ?>

							 		<li> <?= $info;?> </li>

							 	<?php }?>
							 </ul>

						</div>
					</div>
				</div>

				<div class="row text-center">
					<div class="col-md-12">
						<div style="display:none;"><?php echo $gallery; ?></div>
						<br />
						<div class="btn-faleconosco" style="margin-top: 50px;">
							<span>Para contratar nossos serviços, <a href="<?php echo home_url('/contato'); ?>">fale conosco</a>.</span>
						</div>
					</div>
				</div>

				<br />

				<?php endwhile; endif; ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>

<script>
	$(document).ready(function() {
		$('#botao').click(function(e){
			e.preventDefault();
			$('.link-galeria').trigger( "click" );
		});
		$('.popup-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				// titleSrc: function(item) {
				// 	return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
				// }
			}
		});

		$('#botao2').click(function(e){
			e.preventDefault();
			$('.link-galeria2').trigger( "click" );
		});
		$('.popup-gallery2').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				// titleSrc: function(item) {
				// 	return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
				// }
			}
		});
	});
</script>