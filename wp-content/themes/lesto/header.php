<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Lesto
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-52NBPWK');</script>
<!-- End Google Tag Manager -->

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
<?php
	global $configuracao;
	wp_head();
?>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-67616006-11', 'auto');
  ga('send', 'pageview');
</script>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-52NBPWK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- begin olark code -->
<script type="text/javascript" async>
;(function(o,l,a,r,k,y){if(o.olark)return;
r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0];
y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r);
y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)};
y.extend=function(i,j){y("extend",i,j)};
y.identify=function(i){y("identify",k.i=i)};
y.configure=function(i,j){y("configure",i,j);k.c[i]=j};
k=y._={s:[],t:[+new Date],c:{},l:a};
})(window,document,"static.olark.com/jsclient/loader.js");
/* Add configuration calls below this comment */
olark.identify('5330-152-10-3170');</script>
<!-- end olark code -->



	<!-- TOPO -->

	<div class="row topo hidden-sm hidden-xs">



		<!-- LOGOTIPO -->

		<div class="col-md-6">

			<h1><a href="<?php echo home_url('/'); ?>" title="Lesto Escritórios">Lesto Escritórios</a></h1>

		</div>



		<!-- MENU PRINCIPAL -->

		<div class="col-md-6">



			<!-- REDES SOCIAIS -->

			<ul class="redes-sociais">

				<?php if (isset($configuracao['opt-facebook'])&&$configuracao['opt-facebook']!=""):?><li class="facebook"><a href="<?php echo $configuracao['opt-facebook']; ?>" target="_blank" title="Curta nossa fanpage no Facebook"><i class="fa fa-facebook"></i></a></li><?php endif; ?>

				<?php if (isset($configuracao['opt-twitter'])&&$configuracao['opt-twitter']!=""):?><li class="twitter"><a href="<?php echo $configuracao['opt-twitter']; ?>" target="_blank" title="Siga-nos no Twitter"><i class="fa fa-twitter"></i></a></li><?php endif; ?>

				<?php if (isset($configuracao['opt-googlePlus'])&&$configuracao['opt-googlePlus']!=""):?><li class="googlePlus"><a href="<?php echo $configuracao['opt-googlePlus']; ?>" target="_blank" title="Siga-nos no Google Plus"><i class="fa fa-google-plus"></i></a></li><?php endif; ?>

			</ul>



			<!-- MENU PRINCIPAL -->

			<nav class="menu-principal hidden-xs hidden-sm">

				<ul>

					<li id="menu-inicio"><a href="<?php echo home_url('/'); ?>" title="Ir para página inicial">Página Inicial</a></li>

					<li id="menu-planos"><a href="<?php echo home_url('/planos'); ?>" title="Conheça nossos planos">Planos</a></li>

					<li id="menu-salas"><a href="<?php echo home_url('/salas'); ?>" title="Conheça nossas salas">Salas</a></li>

					<!-- <li id="menu-estrutura"><a href="<?php echo home_url('/estrutura'); ?>" title="Conheça nossa estrutura">Estrutura</a></li> -->

					<!-- <li id="menu-vantagens"><a href="<?php echo home_url('/vantagens'); ?>" title="Vantagens Lesto Escritórios">Vantagens</a></li> -->

					<li id="menu-localizacao"><a href="<?php echo home_url('/localizacao'); ?>" title="Faça-nos uma visita">Localização</a></li>

					<li id="menu-dicas"><a href="<?php echo home_url('/dicas'); ?>" title="Nossas dicas">Dicas</a></li>

					<li id="menu-contato"><a href="<?php echo home_url('/contato'); ?>" title="Entre em contato conosco">Contato</a></li>
					<li id="area-cliente">
						<a href="https://lesto.opensev.com.br" title="Área do cliente" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/img/user.svg" alt="Área do cliente">
						</a>
					</li>

				</ul>

			</nav>



		</div>



	</div>

