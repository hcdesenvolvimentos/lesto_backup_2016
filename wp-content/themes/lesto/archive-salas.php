<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lesto
 */

get_header(); ?>

		<!-- CONTATO -->
	<div class="pg-salas internas container">
		<!-- EQUIPE -->
		<?php
			// LOOP DE DIFERENCIAIS		
			$categoriasalas = get_terms('categoriasalas');			
			foreach ( $categoriasalas as $categoriasalas):
				
			
			$salas = new WP_Query(array(
				'post_type'     => 'salas',
				'posts_per_page'   => -1,
				'order' => 'asc',
				'tax_query'     => array(
					array(
						'taxonomy' => 'categoriasalas',
						'field'    => 'slug',
						'terms'    => $categoriasalas->slug,
						)
					)
				)
			);

			

		?>
		<h3 id="trajano"><?php echo $categoriasalas->name ?></h3>
		<div class="row interna-conteudo">
			<div class="col-md-12">
				<table class="rwd-table">
					<tr>
						<th>Descrição da Sala</th>
						<th>Valor por hora para cliente interno</th>
						<th>Valor por hora para cliente externo</th>
					</tr>
					<?php 
						while ( $salas->have_posts() ) : $salas->the_post(); 
							$Lesto_sala_valor1 =  rwmb_meta('Lesto_sala_valor1');
							$Lesto_sala_valor2 =  rwmb_meta('Lesto_sala_valor2');
							$Lesto_img_galeria =  rwmb_meta('Lesto_img_galeria');

							foreach ($Lesto_img_galeria  as $Lesto_img_galeria ) {
								$Lesto_img_galeria = $Lesto_img_galeria;
							}
							// FOTO SALA
							$fotoSala = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoSala = $fotoSala[0];
					?>
					<tr>
						<td data-th="Descrição da Sala"><?php echo get_the_title(); ?>
							<div class="zoom-gallery">
								<a href="<?php echo $Lesto_img_galeria['full_url'] ?>" title="<?php echo get_the_title(); ?>" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo $fotoSala ?>" alt="<?php echo get_the_title(); ?>">
								</a>
							</div>
						</td>
						<td data-th="Valor para cliente interno"><?php echo $Lesto_sala_valor1 ?></td>
						<td data-th="Valor para cliente externo"><?php echo $Lesto_sala_valor2 ?></td>
					</tr>	
					<?php endwhile; wp_reset_query(); ?>				

				</table>
				<p><?php echo $categoriasalas->description ?></p>
			</div>
		</div>
	<?php  endforeach; ?>
		
	</div>

<script>
	$(document).ready(function() {
	$('.zoom-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}

	});


});
</script>
<?php get_footer(); ?>
