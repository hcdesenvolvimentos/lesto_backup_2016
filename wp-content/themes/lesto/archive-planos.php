<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lesto
 */

get_header(); ?>

	<!-- PLANOS -->
	<div class="pg-planos internas container">

		

		<div class="row interna-conteudo">
			<div class="col-md-12">

			<?php if ( have_posts() ) : ?>

				<?php
				$i = 1;
				while ( have_posts() ) : the_post();
				$zebra = ($i%2 == 0) ? 'plano-b' : 'plano-a';
				?>

					<div class="row plano <?php echo $zebra; ?>">
						<div class="col-md-2 plano-icone">

						<?php
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
							$url = $thumb['0'];
						?>
							<img src="<?php echo $url; ?>" width="150" />
						</div>
						<div class="col-md-6 plano-vantagens">
							<h2><?php echo get_the_title(); ?></h2>
							<ul>
								<?php
								foreach (rwmb_meta('Lesto_plano_items') as $item) {
									echo '<li>' . $item . '</li>';
								}
								?>
							</ul>
							<br />
							<!-- <?php if(is_array(rwmb_meta('Lesto_plano_items2'))): ?>
							<p style="font-size: 1.4em;"><?php echo rwmb_meta('Lesto_plano_titulo_items2'); ?></p>
							<ul>
								<?php
								foreach (rwmb_meta('Lesto_plano_items2') as $item) {
									echo '<li style="font-size: 1.4em;">' . $item . '</li>';
								}
								?>
							</ul>
							<?php endif; ?> -->
							<?php if(is_array(rwmb_meta('Lesto_plano_titulo_items2'))): ?>
								<?php
									$arrayTitulos = rwmb_meta('Lesto_plano_titulo_items2');

									foreach ($arrayTitulos as $arrayTitulo) {

								?>
									<p style="font-size: 1.4em;"><?php echo $arrayTitulo; ?></p>

								<?php } ?>
							<?php endif; ?>
						</div>
						<div class="col-md-4 plano-descricao">
							<b><?php echo rwmb_meta('Lesto_plano_valor'); ?><span><?php echo rwmb_meta('Lesto_plano_frequencia'); ?></span></b>
							<p style=""><?php echo  get_the_content(); ?></p>
						</div>
					</div>

				<?php $i++; endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>


			<?php endif; ?>

			<br />
			<div class="btn-faleconosco text-center">
				<span>Para contratar nossos serviços, <a href="<?php echo home_url('/contato'); ?>">fale conosco</a>.</span>
			</div>

			</div>

		</div>
		<p style="    font-size: 20px;"><strong>* Os valores dos planos, descrito acima, são para contratos por prazo indeterminado, podendo ser cancelado a qualquer momento, mediante aviso prévio de 30 dias.</strong> <br><p>
	
			<!-- <p style="    font-size: 20px;"><strong>	Bônus de 02 mensalidades do plano de endereço comercial / fiscal ou ambos.</strong> <br><p>

			<p style="    font-size: 20px;"><strong>	Promoção válida para contratos fechados até dia 31 de Janeiro de 2019.</strong><br><p> -->
<!-- 
			<p style="    font-size: 20px;"><strong>	Condições: Estabelecida em cláusula de contrato entre as partes, da seguinte forma:</strong><br><p>
<br>
	

			<p style="    font-size: 15px;"><strong>“CLÁUSULA XXXX – DO BONUS DA 11ª. E 12ª. MENSALIDADES PARA O ANO DE 2019</strong><br><p>
			<p style="    font-size: 15px;">
			<strong>XXXX.1.</strong> – A CONTRATADA e o CONTRATANTE, estabelecem em comum acordo, de que caso ambos mantenha contrato firmado, vigente e ininterrupto – A CONTRATADA se compromete a conceder um bônus de uma mensalidade no 11º. E 12º. Mês da seguinte forma:
			</p>
			<p style="    font-size: 15px;">
			<strong>XXXX.1.1.</strong> – Exclusivamente para o  <strong>Plano - Cessão do endereço  comercial / Fiscal e ou ambos</strong>, conforme definido em clausula contratual.
			</p>
			<p style="    font-size: 15px;">
			<strong>XXXX.1.2.</strong> – Conforme mencionado no item XXXX.1. – descrito acima – se ambos mantiverem contrato vigente de forma ininterrupto – o bônus da mensalidade referida do plano citado no item 7.1.1. – descrito acima ocorrerá nos meses base de:
			</p>
			<p style="    font-size: 15px;">
			<strong>XXXX.1.3.</strong> – novembro e dezembro de 2019.”
			</p> -->
		

	</div>

<?php get_footer(); ?>
